<?php
	ob_start();
	$pageTitle = 'Mechanical Form';

	$css_files = '<link rel="stylesheet" href="../css/career/mech/mech.css" />';

	include '../init.php';
	include '../dashboard/connect.php';
 
	$js_files = '<script src="../js/career/mech/mech.js"></script>';
	include '../includes/templates/footer.php';
 


	$done = false;
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$errors = array();

		$name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
		$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
		$mobile = filter_var($_POST['mobile'], FILTER_SANITIZE_NUMBER_INT);
		$uni = filter_var($_POST['uni'], FILTER_SANITIZE_STRING);
		$faculty = filter_var($_POST['faculty'], FILTER_SANITIZE_STRING);
		$dep = filter_var($_POST['dep'], FILTER_SANITIZE_STRING);
		$acadimic = filter_var($_POST['academic'], FILTER_SANITIZE_STRING);

		if (!isset($name)) {
			$errors[] = 'Name is required';
		}
		if (!isset($email)) {
			$errors[] = 'Email is required';
		}
		if (!isset($mobile)) {
			$errors[] = 'Mobile Number is required';
		}
		if (!isset($uni)) {
			$errors[] = 'University is required';
		}
		if (!isset($faculty)) {
			$errors[] = 'Faculty is required';
		}
		if (!isset($dep)) {
			$errors[] = 'Department is required';
		}
		if (!isset($acadimic)) {
			$errors[] = 'Acadimic Year is required';
		}

		if (empty($errors)) {
			
			$stmt = $con->prepare("SELECT * FROM career WHERE email = ? LIMIT 1");
			$stmt->execute(array($email));
			$row = $stmt->fetch();
			$count = $stmt->rowCount();

			if ($count == 0) {

				$stmt = $con->prepare("INSERT INTO career(name, email, mobile, uni, faculty, department, acadimic, soft) VALUES(:zname, :zemail, :zmobile, :zuni, :zfaculty, :zdepartment, :zacadimic, :zsoft)");
	            $stmt->execute(array(
	                'zname' => $name,
	                'zemail' => $email,
	                'zmobile' => $mobile,
	                'zuni' => $uni,
	                'zfaculty' => $faculty,
	                'zdepartment' => $dep,
	                'zacadimic' => $acadimic,
	                'zsoft' => 1,
	                ));

	            if ($stmt) {
	            	$done = true;

	            	header("refresh:3; url=http://www.kvectorfoundation.com/");
		        	//exit();

	            } else {
					$errors[] = 'Some Thing went wrong';
				}
			} else {
	            	$stmt = $con->prepare("SELECT * FROM career WHERE email = ? AND soft = 1 LIMIT 1");
					$stmt->execute(array($email));
					$row = $stmt->fetch();
					$count = $stmt->rowCount();
					if ($count == 0) {

						$stmt = $con->prepare("UPDATE career SET soft = 1 WHERE email = ?");
						$stmt->execute(array($email));
						// $row = $stmt->fetch();

						if ($stmt) {
							$done = true;

	            			header("refresh:3; url=http://www.kvectorfoundation.com/");
						}

					} else {
						$errors[] = 'This Emial Is Registered Once';
					}

	            }

			// return true;
		}
	}

?>

<?php ob_end_flush() ?>


	<div class="container">

			<?php 
				if (isset($done)) {
					if ($done) {
						echo '<div style="margin: 130px auto;"><h1>Well Done</h1>
							<p style="font-size: 20px;">Go to <a href="http://www.kvectorfoundation.com/">K vector</a></p></div>
						';
					} else {
			?>
			<?php
				if (isset($errors)) {
					echo '<div class="error">';
			        foreach ($errors as $error) {  
				        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
				          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				            <span aria-hidden="true">&times;</span>
				          </button>';
				            echo '<strong>' . $error . '</strong>';
				            echo '</div>';
			        }
		            echo '</div>';
				}
			?>

		<div class="row">
			<div class="col-md-6 ml-auto mr-auto mt-5">
				<div class="card bg-light mb-3">
					<h2 class="card-header text-center">
						<div class="row">
							<div class="col-2">
								<img src="../images/career/gear.png" alt="gear" class="img-fluid">
							</div>
							<div class="col-8">Mechanical</div>
							<div class="col-2">
								<img src="../images/career/mech.png" alt="mech" class="img-fluid">
							</div>
						</div>
					</h2>
					<div class="card-body">
						<form class="form  pl-3 pr-3 py-3" method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">
							
							<label >Name:</label>
							<div class="input-group mb-3">
							  <input type="text" placeholder="Enter Your Name" class="form-control" name="name" required>
							  <div class="input-group-prepend">
							    <div class="input-group-text">
							      <i class="fa fa-gear fa-lg"></i>
							    </div>
							  </div>
							</div>
							
							<label >Email:</label>
							<div class="input-group mb-3">
							  <input type="Email" placeholder="Enter Your Email" class="form-control" name="email" required>
							  <div class="input-group-prepend">
							    <div class="input-group-text">
							      <i class="fa fa-gear fa-lg"></i>
							    </div>
							  </div>
							</div>
							
							<label >Mobile Number:</label>
							<div class="input-group mb-3">
							  <input type="tel" placeholder="Enter Mobile Number" class="form-control" pattern=".{10,11}[0-9]" name="mobile" required>
							  <div class="input-group-prepend">
							    <div class="input-group-text">
							      <i class="fa fa-gear fa-lg"></i>
							    </div>
							  </div>
							</div>
							
							<label >University:</label>
							<div class="input-group mb-3">
							  <input type="text" placeholder="Enter Your University" class="form-control" name="uni" required>
							  <div class="input-group-prepend">
							    <div class="input-group-text">
							      <i class="fa fa-gear fa-lg"></i>
							    </div>
							  </div>
							</div>
							
							<label >Faculty:</label>
							<div class="input-group mb-3">
							  <input type="text" placeholder="Enter Your Faculty" class="form-control" name="faculty" required>
							  <div class="input-group-prepend">
							    <div class="input-group-text">
							      <i class="fa fa-gear fa-lg"></i>
							    </div>
							  </div>
							</div>
							
							<label >Department:</label>
							<div class="input-group mb-3">
							  <input type="text" placeholder="Enter Your Department" class="form-control" name="dep" required>
							  <div class="input-group-prepend">
							    <div class="input-group-text">
							      <i class="fa fa-gear fa-lg"></i>
							    </div>
							  </div>
							</div>

							<label >Academic Year:</label>
							<div class="input-group mb-3">
							  <input type="text" placeholder="Enter Your Academic Year" class="form-control" name="academic" required>
							  <div class="input-group-prepend">
							    <div class="input-group-text">
							      <i class="fa fa-gear fa-lg"></i>
							    </div>
							  </div>
							</div>
					
							<div class="text-center">
								<button type="submit" class="btn btn-success btn-lg">
									<i class="fa fa-gear fa-lg"></i>
									Go
								</button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	
	</div>

<?php } } ?>